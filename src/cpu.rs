use std::fmt;

use rand;

use crate::bus::Bus;
use rand::Rng;

// `0x200` is the start of most Chip-8 programs.
// Programs beginning at `0x600` are intended for the ETI 660 computer.
pub const PROGRAM_START: u16 = 0x200;

pub struct Cpu {
    vx: [u8; 16],
    pc: u16,
    i: u16,
    ret_stack: Vec<u16>,
    rng: rand::rngs::ThreadRng,
}

impl Cpu {
    pub fn new() -> Cpu {
        Cpu {
            vx: [0; 16],
            pc: PROGRAM_START,
            i: 0,
            ret_stack: Vec::new(),
            rng: rand::thread_rng(),
        }
    }

    pub fn run_instruction(&mut self, bus: &mut Bus) {
        let lo = u16::from(bus.ram_read_byte(self.pc + 1));
        let hi = u16::from(bus.ram_read_byte(self.pc));
        let instruction: u16 = (hi << 8) | lo;

        let nnn = instruction & 0x0FFF;
        let nn = (instruction & 0x0FF) as u8;
        let n = (instruction & 0x00F) as u8;
        let x = ((instruction & 0x0F00) >> 8) as u8;
        let y = ((instruction & 0x00F0) >> 4) as u8;

        // Instructions are 2 bytes long, so increment the `pc` by 2 when necessary.
        match (instruction & 0xF000) >> 12 {
            0x0 => {
                match nn {
                    0xE0 => {
                        // Clear the screen.
                        bus.clear_screen();
                        self.pc += 2;
                    }
                    0xEE => {
                        // Return from a subroutine.
                        let addr = self.ret_stack.pop().unwrap();
                        self.pc = addr;
                    }
                    _ => panic!(
                        "Unrecognized 0x00** instruction {:#X}:{:#X}",
                        self.pc, instruction
                    ),
                }
            }
            0x1 => {
                // `goto NNN`.
                self.pc = nnn;
            }
            0x2 => {
                // Call subroutine at address `NNN`.
                self.ret_stack.push(self.pc + 2);
                self.pc = nnn;
            }
            0x3 => {
                // Skip the next instruction if `Vx` equals `NN`, `if (Vx == NN)`
                let vx = self.read_reg_vx(x);
                if vx == nn {
                    // This advances the counter and then skips the next instruction.
                    self.pc += 4;
                } else {
                    self.pc += 2;
                }
            }
            0x4 => {
                // Skip the next instruction if `Vx != NN`.
                let vx = self.read_reg_vx(x);
                if vx != nn {
                    self.pc += 4;
                } else {
                    self.pc += 2;
                }
            }
            0x5 => {
                // Skip the next instruction if VX equals VY.
                // Usually the next instruction is a jump to skip a code block.
                // `if (Vx == Vy)`.
                let vx = self.read_reg_vx(x);
                let vy = self.read_reg_vx(y);

                if vx == vy {
                    self.pc += 4;
                } else {
                    self.pc += 2;
                }
            }
            0x6 => {
                // Set VX to NN.
                // `Vx = NN`.
                self.write_reg_vx(x, nn);
                self.pc += 2;
            }
            0x7 => {
                // Add NN to VX. (Carry flag is not changed).
                // `Vx += NN`.
                let vx = self.read_reg_vx(x);
                self.write_reg_vx(x, vx.wrapping_add(nn));
                self.pc += 2;
            }
            0x8 => {
                let vx = self.read_reg_vx(x);
                let vy = self.read_reg_vx(y);

                match n {
                    0x0 => {
                        // Set `Vx` to the value of `Vy`.
                        // `Vx = Vy`.
                        self.write_reg_vx(x, vy);
                    }
                    0x2 => {
                        // Set `Vx` to `Vx and Vy` (Bitwise AND operation).
                        // `Vx = Vx & Vy`.
                        self.write_reg_vx(x, vx & vy);
                    }
                    0x3 => {
                        // Set `Vx` to `Vx xor Vy`.
                        // `Vx = Vx ^ Vy`.
                        self.write_reg_vx(x, vx ^ vy);
                    }
                    0x4 => {
                        // Add `Vy` to `Vx`. `Vf` is set to 1 when there's a carry,
                        // 0 when there isn't.
                        // `Vx += Vy`.
                        let sum = u16::from(vx) + u16::from(vy);
                        self.write_reg_vx(x, sum as u8);
                        if sum > 0xFF {
                            // Set `Vf` to 1.
                            self.write_reg_vx(0xF, 1);
                        }
                    }
                    0x5 => {
                        // `Vy` is subtracted from `Vx`. `Vf` is set to 0 when there's a borrow,
                        // 1 when there isn't.
                        // `Vx -= Vy`.
                        let diff: i8 = vx as i8 - vy as i8;
                        self.write_reg_vx(x, diff as u8);
                        if diff < 0 {
                            self.write_reg_vx(0xF, 1);
                        } else {
                            self.write_reg_vx(0xF, 0);
                        }
                    }
                    0x6 => {
                        // Shift `Vy` right by one and copy the result to `Vx`.
                        // `Vf` is set to the value of the least significant bit of `Vy`
                        // before the shift.
                        // `Vx >>= 1`.
                        self.write_reg_vx(0xF, vx & 0x1);
                        self.write_reg_vx(x, vx >> 1);
                    }
                    0x7 => {
                        // Set VX to VY minus VX. VF is set to 0 when there's a borrow,
                        // and 1 when there isn't.
                        // `Vx = Vy - Vx`
                        let diff: i8 = vy as i8 - vx as i8;
                        self.write_reg_vx(x, diff as u8);

                        if diff < 0 {
                            self.write_reg_vx(0xF, 1);
                        } else {
                            self.write_reg_vx(0xF, 0);
                        }
                    }
                    0xE => {
                        // Store the most significant bit of VX in VF and then shift VX to the left by 1.
                        // VF is the most significant bit value.
                        // SHR Vx.
                        // `Vx <<= 1`
                        self.write_reg_vx(0xF, (vx & 0x80) >> 7);
                        self.write_reg_vx(x, vx << 1);
                    }
                    _ => panic!(
                        "Unrecognized 0x8XY* instruction {:#X}:{:#X}",
                        self.pc, instruction
                    ),
                }

                self.pc += 2;
            }
            0x9 => {
                // Skip the next instruction if VX doesn't equal VY. (Usually the next instruction is a jump to skip a code block).
                // `if (Vx != Vy)`
                let vx = self.read_reg_vx(x);
                let vy = self.read_reg_vx(y);

                if vx != vy {
                    self.pc += 4;
                } else {
                    self.pc += 2;
                }
            }
            0xA => {
                // Set `I` to the address `NNN`.
                // `I = NNN`.
                self.i = nnn;
                self.pc += 2;
            }
            0xB => {
                // Jump to the address NNN plus V0.
                // `PC = V0 + NNN`.
                self.pc = u16::from(self.read_reg_vx(0)) + nnn;
            }
            0xC => {
                // Set VX to the result of a bitwise and operation on a random number (Typically: 0 to 255) and NN.
                // `Vx = rand() & NN`
                // `gen_range()` excludes the high value in its range so increase it by 1 to include it.
                let number = self.rng.gen_range(0, 256) as u8;

                self.write_reg_vx(x, number as u8 & nn);
                self.pc += 2;
            }
            0xD => {
                // Draw a sprite at coordinate `(Vx, Vy)` with width 8, height of `N`.
                // `draw(Vx, Vy, N)`
                let vx = self.read_reg_vx(x);
                let vy = self.read_reg_vx(y);
                self.debug_draw_sprite(bus, vx, vy, n);
                self.pc += 2;
            }
            0xE => {
                let key = self.read_reg_vx(x);
                match nn {
                    0x9E => {
                        // Skip the next instruction if the key stored in `Vx` is pressed. Usually the next instruction is a jump to skip a code block.
                        // `if (key() == Vx)`.
                        if bus.is_key_pressed(key) {
                            self.pc += 4;
                        } else {
                            self.pc += 2;
                        }
                    }
                    0xA1 => {
                        // `if (key() != Vx)` then skip the next instruction.
                        if !bus.is_key_pressed(key) {
                            self.pc += 4;
                        } else {
                            self.pc += 2;
                        }
                    }
                    _ => panic!(
                        "Unrecognized 0xEX** instruction {:#X}:{:#X}",
                        self.pc, instruction
                    ),
                }
            }
            0xF => {
                match nn {
                    0x07 => {
                        // Set `Vx` to the value of the delay timer.
                        // `Vx = get_delay()`.
                        self.write_reg_vx(x, bus.get_delay_timer());
                        self.pc += 2;
                    }
                    0x0A => {
                        // A key press is awaited, then stored in `Vx`. (Blocking Operation. All instruction halted until next key event).
                        // `Vx = get_key()`.
                        if let Some(val) = bus.get_key_pressed() {
                            self.write_reg_vx(x, val);
                            self.pc += 2;
                        }
                    }
                    0x15 => {
                        // Set the delay timer to `Vx`.
                        // `delay_timer(Vx)`.
                        bus.set_delay_timer(self.read_reg_vx(x));
                        self.pc += 2;
                    }
                    0x18 => {
                        // Set the sound timer to `Vx`.
                        // `sound_timer(Vx)`.
                        // TODO: Implement sound timer.
                        // Skip this instruction while the sound timer isn't implemented.
                        self.pc += 2;
                    }
                    0x1E => {
                        // Add `Vx` to `I`.
                        // `Vx += I`.
                        let vx = self.read_reg_vx(x);
                        self.i += u16::from(vx);
                        self.pc += 2;
                    }
                    0x29 => {
                        // Set I to the location of the sprite for the character in VX.
                        // Characters 0-F (in hexadecimal) are represented by a 4x5 font.
                        // `I = sprite_addr[Vx]`.
                        // Multiply by 5 because each sprite has 5 lines; each line is 1 byte.
                        self.i = u16::from(self.read_reg_vx(x)) * 5;
                        self.pc += 2;
                    }
                    0x33 => {
                        // Store the binary-coded decimal representation of VX,
                        // with the most significant of three digits at the address in I,
                        // the middle digit at I plus 1,
                        // and the least significant digit at I plus 2.
                        // In other words, take the decimal representation of VX,
                        // place the hundreds digit in memory at location in I,
                        // the tens digit at location I+1,
                        // and the ones digit at location I+2.
                        // `set_BCD(Vx);`.
                        // `*(I + 0) = BCD(3);`.
                        // `*(I + 1) = BCD(2);`.
                        // `*(I + 2) = BCD(1);`.
                        let vx = self.read_reg_vx(x);
                        bus.ram_write_byte(self.i, vx / 100);
                        bus.ram_write_byte(self.i + 1, (vx % 100) / 10);
                        bus.ram_write_byte(self.i + 2, vx % 10);
                        self.pc += 2;
                    }
                    0x55 => {
                        // Store V0 to VX (including VX) in memory starting at address I.
                        // The offset from I is increased by 1 for each value written,
                        // but I itself is left unmodified.
                        // `reg_dump(Vx,&I)`.
                        for index in 0..=x {
                            let value = self.read_reg_vx(index);
                            bus.ram_write_byte(self.i + u16::from(index), value);
                        }
                        self.i += u16::from(x) + 1;
                        self.pc += 2;
                    }
                    0x65 => {
                        // Fill V0 to VX (including VX) with values from memory starting at address I.
                        // The offset from I is increased by 1 for each value written,
                        // but I itself is left unmodified.
                        // `reg_load(Vx, &I)`.
                        for index in 0..=x {
                            let value = bus.ram_read_byte(self.i + u16::from(index));
                            self.write_reg_vx(index, value);
                        }
                        self.i += u16::from(x) + 1;
                        self.pc += 2;
                    }
                    _ => panic!(
                        "Unrecognized 0xF instruction {:#X}:{:#X}",
                        self.pc, instruction
                    ),
                }
            }
            _ => panic!("Unrecognized instruction {:#X}:{:#X}", self.pc, instruction),
        }
    }

    fn debug_draw_sprite(&mut self, bus: &mut Bus, x: u8, y: u8, height: u8) {
        let mut should_set_vf = false;
        for sprite_y in 0..height {
            let b = bus.ram_read_byte(self.i + u16::from(sprite_y));
            if bus.debug_draw_byte(b, x, y + sprite_y) {
                should_set_vf = true;
            }
        }

        if should_set_vf {
            self.write_reg_vx(0xF, 1);
        } else {
            self.write_reg_vx(0xF, 0);
        }
    }

    fn write_reg_vx(&mut self, index: u8, value: u8) {
        self.vx[index as usize] = value;
    }

    fn read_reg_vx(&self, index: u8) -> u8 {
        self.vx[index as usize]
    }
}

impl fmt::Debug for Cpu {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "\npc: {:#X}", self.pc)?;

        write!(f, "vx: ")?;
        for item in &self.vx {
            write!(f, "{:#X} ", *item)?;
        }

        writeln!(f)?;
        writeln!(f, "i: {:#X}", self.i)
    }
}
