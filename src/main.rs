use std::env;
use std::fs::File;
use std::io::Read;

use minifb::{Key, KeyRepeat, Window, WindowOptions};

use chip8::Chip8;

mod bus;
mod chip8;
mod cpu;
mod display;
mod keyboard;
mod ram;

use display::Display;
use std::time::{Duration, Instant};

fn main() {
    let args: Vec<String> = env::args().collect();
    let file_name = match args.len() {
        0 | 1 => "roms/INVADERS",
        _ => args.get(1).unwrap(),
    };
    let mut file = File::open(file_name).unwrap();
    let mut data: Vec<u8> = Vec::new();
    file.read_to_end(&mut data).expect("File not found!");

    const WIDTH: usize = 640;
    const HEIGHT: usize = 320;

    // ARGB buffer.
    let mut buffer: Vec<u32> = vec![0; WIDTH * HEIGHT];

    let mut window = Window::new(
        "Rust CHIP-8 Emulator",
        WIDTH,
        HEIGHT,
        WindowOptions::default(),
    )
    .unwrap_or_else(|e| {
        panic!("Window creation failed: {:?}", e);
    });

    let mut chip8 = Chip8::new();
    chip8.load_rom(&data);

    let mut last_key_update_time = Instant::now();
    let mut last_instruction_run_time = Instant::now();
    let mut last_display_time = Instant::now();

    while window.is_open() && !window.is_key_down(Key::Escape) {
        let keys_pressed = window.get_keys_pressed(KeyRepeat::Yes);
        let key = match keys_pressed {
            Some(keys) => {
                if keys.is_empty() {
                    None
                } else {
                    Some(keys[0])
                }
            }
            None => None,
        };

        let chip8_key = Chip8::get_chip8_keycode_for(key);

        if chip8_key.is_some()
            || ((Instant::now() - last_key_update_time) >= Duration::from_millis(200))
        {
            last_key_update_time = Instant::now();
            chip8.set_key_pressed(chip8_key);
        }

        if (Instant::now() - last_instruction_run_time) > Duration::from_millis(2) {
            chip8.run_instruction();
            last_instruction_run_time = Instant::now();
        }

        if (Instant::now() - last_display_time) > Duration::from_millis(10) {
            let chip8_buffer = chip8.get_display_buffer();

            for y in 0..HEIGHT {
                let coord_y = y / 10;
                let offset = y * WIDTH;

                for x in 0..WIDTH {
                    let index = Display::get_index_from_coords(x / 10, coord_y);
                    let pixel = chip8_buffer[index];
                    let color_pixel = match pixel {
                        0 => 0x0,        // Black
                        1 => 0xff_ff_ff, // White
                        _ => unreachable!(),
                    };
                    buffer[offset + x] = color_pixel;
                }
            }

            // We unwrap here as we want this code to exit if it fails. Real applications may want to handle this in a different way.
            window.update_with_buffer(&buffer).unwrap();
            last_display_time = Instant::now();
        }
    }
}
