# CHIP-8 Emulator

This is a CHIP-8 emulator implemented in Rust. Some sample ROMs are included.

## Table of Contents

* [ROMs](#roms)
* [How To Play](#how-to-play)

## ROMs

The ROMs included in this project are public domain.
They were downloaded from [Zophar's Domain](https://www.zophar.net/pdroms/chip8/chip-8-games-pack.html).

# How To Play

The entire CHIP-8 keyboard is mapped like this:

CHIP-8 keyboard:

| | | | |
|-|-|-|-|
|1|2|3|C|
|4|5|6|D|
|7|8|9|E|
|A|0|B|F|

Is mapped to (QWERTY layout):

| | | | |
|-|-|-|-|
|1|2|3|4|
|Q|W|E|R|
|A|S|D|F|
|Z|X|C|V|
